# Run Security Jobs Sequentially

For this particular example in this project, SAST, Infrastructure as a Code scanning and Secret detection scanning have been enabled. Note, the same procedures below will apply to other GitLab security scanners as well. 

Override the correspond job for each scanner and use the [needs](https://docs.gitlab.com/ee/ci/yaml/#needs) keyword in order to execute jobs out-of-order.

To override a job definition, declare a job with the same name as the security scanner job that you need to override. Place this new job after the template inclusion and specify any additional keys under it. 

You can [view the job dependecies in the pipeline graph](https://docs.gitlab.com/ee/ci/pipelines/index.html#view-job-dependencies-in-the-pipeline-graph) by toggling _**Show dependencies**_. 


![](./images/showDependencies.png)


